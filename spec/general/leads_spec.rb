require 'spec_helper'

def get_created_lead_from_list name
  on(NavigationPage).leads
  on(LeadsPage).get_lead name
end

shared_examples "Create leads and check it" do |last_name, note_content, first_name, 
                  company, title, lead_status, email, phone, country, tag, source, 
                  street, city, state, zip|
  it "Open add lead form" do
    on(LeadsPage).add_new_lead_element.when_visible.click
  end

  it "Add lead first name", :if => first_name do
    on(LeadsPage).first_name_element.when_visible(10).value=first_name
  end

  it "Add lead last name", :if => last_name do
    on(LeadsPage).last_name_element.when_visible(10).value=last_name
  end

  it "Add company name", :if => company do
    on(LeadsPage).company_name_element.when_visible(10).value=company
  end

  it "Add title", :if => title do
    on(LeadsPage).title_element.when_visible(10).value=title
  end

  it "Add lead status", :if => lead_status do
    on(LeadsPage).change_lead_status lead_status
  end

  it "Add email address", :if => email do
    on(LeadsPage).email_element.when_visible(10).value=email
  end

  it "Add mobile phone", :if => phone do
    on(LeadsPage).mobile_phone_element.when_visible(10).value=phone
  end

  it "Add address", :if => street do
    on(LeadsPage).address_element.when_visible(10).value=street
  end

  it "Add city name", :if => city do
    on(LeadsPage).city_element.when_visible(10).value=city
  end

  it "Add state name", :if => state do
    on(LeadsPage).state_element.when_visible(10).value=state
  end

  it "Add zip code", :if => zip do
    on(LeadsPage).zip_element.when_visible(10).value=zip
  end

  it "Select country", :if => country do
    on(LeadsPage).select_country country
  end

  it "Add tag", :if => tag do
    on(LeadsPage).add_tag tag
  end

  # it "Add source", :if => source do
  #   on(LeadsPage).add_source source
  # end

  it "Save new lead" do
    on(LeadsPage).save_lead
  end

  it "Add note content", :if => note_content do
    on(LeadsPage).note_content_element.when_visible(10).value=note_content
  end

  it "Save note" do
    on(LeadsPage).save_note
  end

  it "Verify presence of activity list" do
    on(LeadsPage).notes_list_element.when_visible(10)
  end

  it "Verify last note" do
    notes = on(LeadsPage).get_notes 
    expect(notes[0]).not_to be_nil
    expect(notes[0].text).to include(note_content)
  end

  it "Verify lead on lead's list", :if => last_name do
    on(NavigationPage).leads
    on(LeadsPage).filter_leads_element.value = last_name
    sleep 2.seconds
    on(LeadsPage).leads_list_element.when_visible(10).list_item_elements.each do |item|
      item.click
    end
    # sleep 5.seconds
  end

  it "Verify lead content", :if => last_name, :if => lead_status, :if => phone, :if => email do
    expect(on(LeadsPage).lead_title_element.when_visible(30).text).to include(last_name)
    expect(on(LeadsPage).details.split("\n")[2]).to include(lead_status)
    expect(on(LeadsPage).details.split("\n")[3]).to include(phone)
    expect(on(LeadsPage).details.split("\n")[4]).to include(email)
  end
end

describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end

  describe "Add floating lead" do
    include_examples "Create leads and check it", Faker::Lorem.sentence, Faker::Lorem.sentence, 'Name',
                      'Moja Firma', 'Moj tytul', 'Working', 'email@gmail.com', '555', 'Poland', 'tag1',
                      'kasia_source'
  end
end
