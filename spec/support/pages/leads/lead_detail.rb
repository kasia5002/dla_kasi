module LeadDetail 
  include PageObject

  textarea(:note_content, :name => "note")
  button(:save_note, :text => "Save")
  div(:notes_list, :css => ".activities-list")
  div(:activity_content, :css => ".note-content")
  span(:lead_title, :css => ".detail-title")
  div(:details, :css => ".object-details .phone")
  


  def get_notes 
    notes = []
    notes_list_element.when_visible.list_item_elements.each do |item|
      notes << item 
    end
    notes
  end
end