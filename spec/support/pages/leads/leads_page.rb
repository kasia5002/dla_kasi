class LeadsPage < AbstractPage
  include PageObject
  include LeadForm
  include LeadDetail
  include RelatedToPicker

  page_url "https://app.futuresimple.com/leads"

  link(:add_new_lead, :id => "leads-new")
  div(:leads_list, :css => ".leads")
  text_field(:filter_leads, :placeholder => "Filter Leads")

end