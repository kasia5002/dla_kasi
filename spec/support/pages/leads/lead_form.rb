module LeadForm
  include PageObject

  text_field(:first_name, :name => "first_name", :placeholder => "First Name")
  text_field(:last_name, :name => "last_name", :placeholder => "Last Name*")
  text_field(:company_name, :name => "company_name")
  text_field(:title, :name => "title")
  text_field(:email, :name => "email")
  text_field(:mobile_phone, :name => "mobile")
  text_field(:work_phone, :name => "phone")
  text_field(:address, :name => "street")
  text_field(:city, :name => "city")
  text_field(:zip, :name => "zip")
  text_field(:state, :name => "region")
  div(:country, :css => ".control-group:nth-of-type(10)")
  button(:save_lead, :text => "Save")



  def change_lead_status lead_status
    self.div_element(:css => ".control-group:nth-of-type(4)").span_element.click
    self.div_element(:css => ".control-group:nth-of-type(4)").text_field_element.send_keys lead_status
    self.div_element(:css => ".control-group:nth-of-type(4) .chzn-drop").list_item_elements[0].when_visible(5).text.include? lead_status
    self.div_element(:css => ".control-group:nth-of-type(4)").text_field_element.send_keys :enter
    self.link_element(:text => "#{lead_status}").when_visible(5).visible?
  end

  def select_country country
    self.div_element(:css => ".control-group:nth-of-type(10)").span_element.click
    self.div_element(:css => ".control-group:nth-of-type(10)").text_field_element.send_keys country
    self.div_element(:css => ".control-group:nth-of-type(10)").list_item_elements[0].when_visible(5).text.include? country
    self.div_element(:css => ".control-group:nth-of-type(10)").text_field_element.send_keys :enter
    self.link_element(:text => "#{country}").when_visible(5).visible?
  end

  def add_tag tag
    self.div_element(:css => ".tags-field .search-field").click
    self.div_element(:css => ".tags-field .search-field").text_field_element.send_keys tag
    self.div_element(:css => ".tags-field .search-field").text_field_element.send_keys :enter
    self.list_item_element(:text => "#{tag}").when_visible(5).visible?
  end

  def add_source source
    self.div_element(:css => ".source-select-field").click
    self.div_element(:css => ".source-select-field").text_field_element.send_keys "Add a new source"
    self.div_element(:css => ".source-select-field").text_field_element.send_keys :enter
    self.div_element(:css => ".modal-body").text_field_element.when_visible(10).send_keys source
    self.div_element(:css => ".modal-body").text_field_element.text.include? source
    self.button_element(:text => "OK").click
    sleep 3.seconds
  end
  
end